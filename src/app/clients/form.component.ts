import { Component, OnInit } from '@angular/core';
import { Client } from './client';
import { ClientService } from './client.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {

  public client: Client = new Client();
  public title: string = 'Crear Cliente';

  constructor(private clientService: ClientService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadClient();
  }

  // Carga el cliente recibe como parametro el id, si recibe el id devuelve el cliente actual.
  loadClient(): void {
    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.clientService.getClient(id).subscribe(
          (client) => this.client = client
        );
      }
    });
  }
 // Crea un cliente
  create(): void {
    this.clientService.create(this.client).subscribe(
      client => {
        this.router.navigate(['/clients']);
        swal('Nuevo cliente', `Cliente ${client.name} creado con éxito.`, 'success');
      }
    );
    console.log('Clicked!');
    console.log(this.client);
  }

  update(): void {
    this.clientService.updateClient(this.client)
    .subscribe(client => {
      this.router.navigate(['/clients']);
      swal('Cliente Actualizado', `Cliente ${client.name} se ha actualizado con éxito.`, 'success');
    });
  }

}
