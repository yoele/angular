import { Injectable } from '@angular/core';
import { Client } from './client';
import { of, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class ClientService {

  private urlEndPoint: string = 'http://localhost:8080/api/clients';
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  // Trae todos cliente desde la base de datos, con Observable esta actuando de forma asincronica
  getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(this.urlEndPoint);
  }

  // Crea el cliente en nuestra base de datos, con Observable esta actuando de forma asincronica
  create(client: Client): Observable<Client> {
    return this.http.post<Client>(this.urlEndPoint, client, { headers: this.httpHeaders });
  }

  // Trae el cliente con el {id} mencionado desde la base de datos, con Observable esta actuando de forma asincronica
  getClient(id): Observable<Client> {
    return this.http.get<Client>(`${this.urlEndPoint}/${id}`);
  }

  // Actualiza el cliente mediante su id, recibe un cliente, lo modifica y lo devuelve, es de tipo observable
  updateClient(client: Client): Observable<Client> {
    return this.http.put<Client>(`${this.urlEndPoint}/${client.id}`, client, { headers: this.httpHeaders });
  }

  delete(id: number): Observable<Client> {
    return this.http.delete<Client>(`${this.urlEndPoint}/${id}`, { headers: this.httpHeaders });
  }

}
