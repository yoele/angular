import { Client } from './client';

export const CLIENTS: Client[] = [
  { id: 1, name: 'Yoel', lastName: 'Estrada', createdAt: '2020-05-25', email: 'yoel@estrada.com'},
  { id: 2, name: 'Tomas', lastName: 'Toloza', createdAt: '2020-03-19', email: 'tomas@toloza.com'},
  { id: 3, name: 'Dario', lastName: 'Gomez', createdAt: '2019-12-10', email: 'dario@gomez.com'},
  { id: 4, name: 'Hernan', lastName: 'Nagorny', createdAt: '2020-03-03', email: 'herna@nagorny.com'},
  { id: 5, name: 'Matias', lastName: 'Katz', createdAt: '2020-01-29', email: 'matias@katz.com'}
];
